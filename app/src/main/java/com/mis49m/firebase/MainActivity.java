package com.mis49m.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    TextView txt;
    RecyclerView recyclerView;

    ContactAdapter contactAdapter;
    ArrayList<Contact> contacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt = findViewById(R.id.txt);
        recyclerView = findViewById(R.id.rv);

        contactAdapter = new ContactAdapter(contacts, MainActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(contactAdapter);

        /*
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("valueEventListener", dataSnapshot.toString());
                contacts.clear();
                for (DataSnapshot s : dataSnapshot.getChildren()) {
                    Contact contact = s.getValue(Contact.class);
                    contacts.add(contact);
                }
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("Cancelled", databaseError.toException());
            }
        };
        database.getReference().child("contacts").addValueEventListener(valueEventListener);
*/

        //child event listener
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.i("onChildAdded", dataSnapshot.toString());
                Contact contact = dataSnapshot.getValue(Contact.class);
                contact.setKey(dataSnapshot.getKey());
                contacts.add(contact);
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.i("onChildChanged", dataSnapshot.toString());
                int i = 0;
                for (Contact contact : contacts) {
                    if (contact.key.equals(dataSnapshot.getKey())) {
                        contacts.set(i, dataSnapshot.getValue(Contact.class));
                        break;
                    }
                    i++;
                }
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.i("onChildRemoved", dataSnapshot.toString());
                for (Contact contact : contacts) {
                    if (contact.key.equals(dataSnapshot.getKey())) {
                        contacts.remove(contact);
                        break;
                    }
                }
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        database.getReference().child("contacts").child(firebaseAuth.getCurrentUser().getUid()).addChildEventListener(childEventListener);


        /* Read single value
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()!=null) {
                    Contact contact = dataSnapshot.getValue(Contact.class);
                    txt.setText(contact.getName());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        database.getReference().child("contact").addValueEventListener(valueEventListener);
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(this, AddActivity.class);
                startActivity(intent);
                break;

            case R.id.sign_out:
                FirebaseAuth.getInstance().signOut();
                Intent signOutIntenet = new Intent(this, LoginActivity.class);
                startActivity(signOutIntenet);
                finish();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }

        return false;
    }

}
