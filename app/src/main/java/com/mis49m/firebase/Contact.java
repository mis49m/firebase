package com.mis49m.firebase;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

public class Contact implements Serializable {

    @Exclude
    String key;

    String name;
    String phone;

    public Contact() {

    }

    public Contact(String key, String name, String phone) {
        this.key = key;
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Exclude
    public String getKey() {
        return key;
    }

    @Exclude
    public void setKey(String key) {
        this.key = key;
    }
}
