package com.mis49m.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddActivity extends AppCompatActivity {

    public static String CONTACT = "Contact";

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();


    EditText etName, etPhone;
    Button btnSave;

    Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        etName = findViewById(R.id.et_name);
        etPhone = findViewById(R.id.et_phone);

        Intent intent = getIntent();
        if ( intent.hasExtra(CONTACT) ) {
            contact = (Contact) intent.getSerializableExtra(CONTACT);
            etName.setText(contact.getName());
            etPhone.setText(contact.getPhone());
        } else {
            contact = new Contact();
        }

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contact.setName(etName.getText().toString());
                contact.setPhone(etPhone.getText().toString());

                DatabaseReference myRef = database.getReference();
                if (contact.getKey()==null) {
                    //myRef.child("contact").child("name").setValue(etName.getText().toString());
                    //myRef.child("contact").child("phone").setValue(etPhone.getText().toString());

                    //myRef.child("contact").setValue(contact);

                    myRef.child("contacts").child(firebaseAuth.getCurrentUser().getUid()).push().setValue(contact);
                } else {
                    myRef.child("contacts").child(firebaseAuth.getCurrentUser().getUid()).child(contact.getKey()).setValue(contact);
                }

                finish();
            }
        });

    }
}
